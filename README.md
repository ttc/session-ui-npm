
## Session UI React NPM package

This is the repo for the Session UI NPM package and holds the React part of the Session UI application.

See https://gitlab.com/ttc/session-ui/

### Installation

npm install

npm run build
